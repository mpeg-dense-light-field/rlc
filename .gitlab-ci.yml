variables:
  OPENCV_VERSION: "4.10.0"
  VERSION: '40'
  VERSION_COMMIT: '${CI_COMMIT_TAG:-${CI_COMMIT_SHA:0:8}}'

stages:
  - prepare
  - build
#  - test
  - package
  - release

prepare_opencv:
  stage: prepare
  image: debian:12-slim
  script:
    - apt-get update && apt-get install -y curl
    - curl -L https://github.com/opencv/opencv/archive/refs/tags/${OPENCV_VERSION}.tar.gz -o opencv.tar.gz
    - curl -L https://github.com/opencv/opencv_contrib/archive/refs/tags/${OPENCV_VERSION}.tar.gz -o opencv_contrib.tar.gz
  artifacts:
    paths:
      - opencv.tar.gz
      - opencv_contrib.tar.gz
    expire_in: 1 day

build_linux:
  stage: build
  image: silkeh/clang:19
  script:
    - apt-get update && apt-get install -y --no-install-recommends git cmake make
    - tar xf opencv.tar.gz
    - tar xf opencv_contrib.tar.gz
    - cd opencv-${OPENCV_VERSION}
    - cmake -S . -B build 
      -DBUILD_LIST="imgcodecs,imgproc,quality" 
      -DBUILD_SHARED_LIBS=OFF 
      -DCV_TRACE=OFF 
      -DCPU_BASELINE=AVX2 
      -DCPU_DISPATCH=AVX2 
      -DOPENCV_ENABLE_ALLOCATOR_STATS=OFF 
      -DWITH_ADE=OFF 
      -DWITH_DSHOW=OFF 
      -DWITH_FFMPEG=OFF 
      -DWITH_IMGCODEC_HDR=OFF 
      -DWITH_IMGCODEC_PFM=OFF 
      -DWITH_IMGCODEC_PXM=OFF 
      -DWITH_IMGCODEC_SUNRASTER=OFF 
      -DWITH_IPP=OFF 
      -DWITH_ITT=OFF 
      -DWITH_JASPER=OFF 
      -DWITH_JPEG=OFF 
      -DWITH_LAPACK=OFF 
      -DWITH_OPENCL=OFF 
      -DWITH_OPENEXR=OFF 
      -DWITH_OPENJPEG=OFF 
      -DWITH_PROTOBUF=OFF 
      -DWITH_TIFF=OFF 
      -DWITH_WEBP=OFF 
      -DOPENCV_EXTRA_MODULES_PATH=../opencv_contrib-${OPENCV_VERSION}/modules
    - make -C build -j$(nproc)
    - make -C build install
    - cd ..
    - cmake -S . -B build -DCMAKE_BUILD_TYPE=Release
    - cmake --build build --config Release --parallel $(nproc) --target RLC40
    - mkdir -p linux
    - cp build/RLC40 linux/RLC${VERSION}
  artifacts:
    paths:
      - linux/
  dependencies:
    - prepare_opencv

#build_macos:
#  stage: build
#  tags:
#    - macos
#  script:
#    - brew install cmake
#    - tar xf opencv.tar.gz
#    - tar xf opencv_contrib.tar.gz
#    - cd opencv-${OPENCV_VERSION}
#    - cmake -S . -B build 
#      -DBUILD_LIST="imgcodecs,imgproc,quality" 
#      -DBUILD_SHARED_LIBS=OFF 
#      -DCV_TRACE=OFF 
#      -DCPU_BASELINE=AVX2 
#      -DCPU_DISPATCH=AVX2 
#      -DOPENCV_ENABLE_ALLOCATOR_STATS=OFF 
#      -DWITH_ADE=OFF 
#      -DWITH_DSHOW=OFF 
#      -DWITH_FFMPEG=OFF 
#      -DWITH_IMGCODEC_HDR=OFF 
#      -DWITH_IMGCODEC_PFM=OFF 
#      -DWITH_IMGCODEC_PXM=OFF 
#      -DWITH_IMGCODEC_SUNRASTER=OFF 
#      -DWITH_IPP=OFF 
#      -DWITH_ITT=OFF 
#      -DWITH_JASPER=OFF 
#      -DWITH_JPEG=OFF 
#      -DWITH_LAPACK=OFF 
#      -DWITH_OPENCL=OFF 
#      -DWITH_OPENEXR=OFF 
#      -DWITH_OPENJPEG=OFF 
#      -DWITH_PROTOBUF=OFF 
#      -DWITH_TIFF=OFF 
#      -DWITH_WEBP=OFF 
#      -DOPENCV_EXTRA_MODULES_PATH=../opencv_contrib-${OPENCV_VERSION}/modules
#    - make -C build -j$(sysctl -n hw.ncpu)
#    - sudo make -C build install
#    - cd ..
#    - cmake -S . -B build -DCMAKE_BUILD_TYPE=Release
#    - cmake --build build --config Release --parallel $(sysctl -n hw.ncpu) --target RLC40
#    - mkdir -p macos
#    - cp build/RLC40 macos/RLC${VERSION}
#  artifacts:
#    paths:
#      - macos/
#  dependencies:
#    - prepare_opencv

build_windows:
  stage: build
  tags:
    - saas-windows-medium-amd64
  script:
    - tar -xf opencv.tar.gz
    - tar -xf opencv_contrib.tar.gz
    - cd opencv-${OPENCV_VERSION}
    - cmake -S . -B build -G "Visual Studio 17 2022" -A x64 
      -DCMAKE_INSTALL_PREFIX="C:/opencv"
      -DOPENCV_ENABLE_NONFREE=ON
      -DBUILD_LIST="imgcodecs,imgproc,core,features2d,calib3d,quality"
      -DBUILD_SHARED_LIBS=ON
      -DCV_TRACE=OFF 
      -DCPU_BASELINE=AVX2 
      -DCPU_DISPATCH=AVX2 
      -DOPENCV_ENABLE_ALLOCATOR_STATS=OFF 
      -DWITH_ADE=OFF 
      -DWITH_DSHOW=OFF 
      -DWITH_FFMPEG=OFF 
      -DWITH_IMGCODEC_HDR=OFF 
      -DWITH_IMGCODEC_PFM=OFF 
      -DWITH_IMGCODEC_PXM=OFF 
      -DWITH_IMGCODEC_SUNRASTER=OFF 
      -DWITH_IPP=OFF 
      -DWITH_ITT=OFF 
      -DWITH_JASPER=OFF 
      -DWITH_JPEG=OFF 
      -DWITH_LAPACK=OFF 
      -DWITH_OPENCL=OFF 
      -DWITH_OPENEXR=OFF 
      -DWITH_OPENJPEG=OFF 
      -DWITH_PROTOBUF=OFF 
      -DWITH_TIFF=OFF 
      -DWITH_WEBP=OFF 
      -DOPENCV_EXTRA_MODULES_PATH="../opencv_contrib-${OPENCV_VERSION}/modules"
      -DBUILD_opencv_quality=ON
    - cmake --build build --config Release --parallel $env:NUMBER_OF_PROCESSORS --verbose
    - cmake --install build
    - cd ..
    - cmake -S . -B build -G "Visual Studio 17 2022" -A x64 
      -DCMAKE_BUILD_TYPE=Release
      -DOpenCV_DIR="C:/opencv"
    - cmake --build build --config Release --parallel $env:NUMBER_OF_PROCESSORS --target RLC40
    - mkdir windows
    - cp build\Release\RLC40.exe windows\RLC${VERSION}.exe
    - cp C:/opencv/x64/vc17/bin/*.dll windows\
  artifacts:
    paths:
      - windows/
  dependencies:
    - prepare_opencv

#test:
#  stage: test
#  image: debian:12-slim
#  script:
#    - chmod +x linux/RLC-${VERSION}
#    - ./linux/RLC${VERSION} --version
#  dependencies:
#    - build_linux

package:
  stage: package
  image: debian:12-slim
  script:
    - apt-get update && apt-get install -y zip
    - VERSION_COMMIT=${VERSION_COMMIT:-default}  # Fallback if not set
    - echo "VERSION_COMMIT=${VERSION_COMMIT}"
    - echo "${VERSION_COMMIT}" > version.txt
    #- zip -r "RLC${VERSION_COMMIT}-all-platforms.zip" linux/ macos/ windows/ version.txt
    - zip -r "RLC-all-platforms.zip" linux/ windows/ version.txt
  artifacts:
    paths:
      - "RLC-all-platforms.zip"
    expire_in: never
  dependencies:
    - build_linux
    #- build_macos
    - build_windows

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    #- when: always
    - if: $CI_COMMIT_TAG
  script:
    - echo "Creating release ${CI_COMMIT_TAG}"
  release:
    name: "Release ${CI_COMMIT_TAG}"
    description: "Release ${CI_COMMIT_TAG}"
    tag_name: "${CI_COMMIT_TAG}"
    assets:
      links:
        - name: "RLC${CI_COMMIT_TAG}-all-platforms.zip"
          url: "${CI_PROJECT_URL}/-/jobs/artifacts/${CI_COMMIT_TAG}/raw/RLC-all-platforms.zip?job=package"